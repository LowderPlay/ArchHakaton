<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Socialhack | 3 измерения</title>
    <meta name="description" content="Socialhack | 3 измерения">
    <script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>
    <script src="//cdn.rawgit.com/donmccurdy/aframe-physics-system/v3.3.0/dist/aframe-physics-system.min.js"></script>
    <script src="https://cdn.rawgit.com/donmccurdy/aframe-extras/v4.1.2/dist/aframe-extras.min.js"></script>
    <script src="https://rawgit.com/fernandojsg/aframe-teleport-controls/master/dist/aframe-teleport-controls.min.js"></script>
    <script src="https://rawgit.com/rdub80/aframe-teleport-extras/master/dist/aframe-teleport-extras.min.js"></script>
  	<script src="https://unpkg.com/aframe-environment-component@1.1.0/dist/aframe-environment-component.min.js"></script>
  	<script src="https://cdn.statically.io/gh/darkwave/aframe-stl-model-component/7ed9bfa9/dist/aframe-stl-model-component.min.js"></script>
  	<script type="text/javascript" src="https://cdn.statically.io/gh/supermedium/superframe/897baf70/components/aabb-collider/dist/aframe-aabb-collider-component.min.js"></script>
  	<script type="text/javascript" src="https://cdn.statically.io/gh/LowderPlay/Fade/f321c8e3/fade.js"></script>
  </head>
  <body>

    <a-scene id="main" background="color: #ffffff">
    	<a-entity light="type: ambient; intensity: 0.32; color: #ffffff"></a-entity>
          <a-entity environment="preset: forest"></a-entity>
        <a-assets>
			<a-asset-item response-type="arraybuffer" id="type_f" src="ячейка_типF_v13.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="type_f_2" src="НОВАЯ_ЯЧЕЙКА_v1.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="type_f_door" src="dooooooor.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="type_f_floor" src="ПОЛЯЧЕЙКИ.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="sink" src="Lavabo.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="toilet" src="realistic_toilet.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="bath" src="wanna1.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="cabinet" src="oldcabinet.max_1__1_.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="chair" src="chair.stl"></a-asset-item>
			<a-asset-item response-type="arraybuffer" id="table" src="stoll.stl"></a-asset-item>
			<img id="image-1" src="imgs/aaa.jpg">
			<img id="image-2" src="imgs/downstairs-view.jpg">
			<img id="image-3" src="imgs/photo.jpg">
			<img id="image-4" src="imgs/print1.jpg">
			<img id="image-5" src="imgs/print2.JPG">
        </a-assets>


        <a-entity id="main_scene" visible="true"> 
        	<!-- Комната-ячейка -->
            <a-entity id="body" stl-model="src: #type_f" scale="0.001 0.001 0.001" rotation="0 0 0" position="0 1 0" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="body_d" stl-model="src: #type_f_door" scale="0.001 0.001 0.001" rotation="0 90 0" position="4.6 1 12.9" material="color: #b7b7b7; roughness: 1; metalness: 0; opacity:0.5;"></a-entity>
            <a-entity id="body_F" stl-model="src: #type_f_floor" opacity="0" scale="0.001 0.001 0.001" rotation="0 0 0" position="0 1 0" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-image src="#image-1" depth="0.1" position="1.15 3 3.34" rotation="0 180 0" scale="1 1.300 1a"></a-image>
            <a-box id="button_bio" aabb-collider="objects: .hand" position="1.15 2.2 3.34" rotation="0 0 0" color="#ff5559" scale="0.6 0.2 0.2"></a-box>
            <!-- Мебель в сан. узле -->
            <a-entity id="sink_obj" stl-model="src: #sink" opacity="0" scale="0.01 0.01 0.01" rotation="0 180 0" position="7.95 3.5 4.5" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="bath_obj" stl-model="src: #bath" opacity="0" scale="0.007 0.007 0.007" rotation="-90 0 0" position="8.7 3.95 3.03" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="toilet_obj" stl-model="src: #toilet" opacity="0" scale="0.01 0.01 0.01" rotation="0 180 0" position="7.19 3.9 4.2" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>

            <!-- Другая мебель -->
            <a-entity id="cabinet_obj" stl-model="src: #cabinet" opacity="0" scale="0.01 0.01 0.01" rotation="-90 0 0" position="1 1 0.25" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="cabinet_obj2" stl-model="src: #cabinet" opacity="0" scale="0.01 0.01 0.01" rotation="-90 -180 0" position="6.8 1 3.07" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="chair_obj" stl-model="src: #chair" opacity="0" scale="0.01 0.01 0.01" rotation="-90 90 0" position="6.85 1.48 1.56" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="table_obj" stl-model="src: #table" opacity="0" scale="0.01 0.01 0.01" rotation="-90 180 0" position="7.45 1 1.5" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>

            <a-entity id="images_gallery" material="opacity: 0;">
            	<a-image class="gallery_item" src="#image-2" depth="0.1" position="3 3 2" rotation="0 45 0" scale="1 1.300 1"></a-image>
            	<a-image class="gallery_item" src="#image-3" depth="0.1" position="2.2 3 2.3" rotation="0 0 0" scale="1 1.300 1"></a-image>
            	<a-image class="gallery_item" src="#image-4" depth="0.1" position="0.4 3 2" rotation="0 -45 0" scale="1 1.300 1"></a-image>
            	<a-image class="gallery_item" src="#image-5" depth="0.1" position="1.15 3 2.3" rotation="0 0 0" scale="1 1.300 1"></a-image>
            </a-entity>
        </a-entity>
        <a-entity id="second_scene" visible="false">
        	<!-- Комната-ячейка -->
            <a-entity id="body" stl-model="src: #type_f_2" scale="0.001 0.001 0.001" rotation="0 0 0" position="0 1 0" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="body_d" stl-model="src: #type_f_door" scale="0.001 0.001 0.001" rotation="0 90 0" position="4.6 1 12.9" material="color: #b7b7b7; roughness: 1; metalness: 0; opacity:0.5;"></a-entity>
            <a-entity id="body_F" stl-model="src: #type_f_floor" opacity="0" scale="0.001 0.001 0.001" rotation="0 0 0" position="0 1 0" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-image src="#image-1" depth="0.1" position="1.15 3 3.34" rotation="0 180 0" scale="1 1.300 1a"></a-image>
            <a-box id="button_bio" aabb-collider="objects: .hand" position="1.15 2.2 3.34" rotation="0 0 0" color="#ff5559" scale="0.6 0.2 0.2"></a-box>
            <!-- Мебель в сан. узле -->
            <a-entity id="sink_obj" stl-model="src: #sink" opacity="0" scale="0.01 0.01 0.01" rotation="0 180 0" position="7.95 3.5 4.5" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="bath_obj" stl-model="src: #bath" opacity="0" scale="0.007 0.007 0.007" rotation="-90 0 0" position="8.7 3.95 3.03" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="toilet_obj" stl-model="src: #toilet" opacity="0" scale="0.01 0.01 0.01" rotation="0 180 0" position="7.19 3.9 4.2" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>

            <!-- Другая мебель -->
            <a-entity id="cabinet_obj" stl-model="src: #cabinet" opacity="0" scale="0.01 0.01 0.01" rotation="-90 0 0" position="1 1 0.25" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="cabinet_obj2" stl-model="src: #cabinet" opacity="0" scale="0.01 0.01 0.01" rotation="-90 -180 0" position="6.8 1 3.07" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="chair_obj" stl-model="src: #chair" opacity="0" scale="0.01 0.01 0.01" rotation="-90 0 0" position="6.85 1.48 0.57" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
            <a-entity id="table_obj" stl-model="src: #table" opacity="0" scale="0.01 0.01 0.01" rotation="-90 90 0" position="7.45 1 1.5" material="color: #b7b7b7; roughness: 1; metalness: 0"></a-entity>
        </a-entity>

        <a-entity id="cameraRig" position="1.15 0 0.5">
	      <!-- camera -->
	      <a-entity id="head" camera wasd-controls look-controls></a-entity>
	      <!-- hand controls -->
	      <a-entity class="hand" static-body vive-controls="hand: left" id="left-hand" teleport-controls="cameraRig: #cameraRig; teleportOrigin: #head; collisionEntities: #body_F;"  ></a-entity>
	      <a-entity class="hand" static-body vive-controls="hand: right" id="right-hand" teleport-controls="cameraRig: #cameraRig; teleportOrigin: #head; collisionEntities: #body_F;" ></a-entity>
	    </a-entity>
        
    </a-scene>
    <script type="text/javascript">
    	var audio = document.createElement('audio');
    	var voiceovers = {
    		bio: "Моисей Яковлевич Гинзбург (родился 23 мая 1892 в Минске — умер 7 января 1946 в Москве) — советский архитектор, практик, теоретик и один из лидеров конструктивизма. В 1923 году опубликовал доклад «Ритм в архитектуре», а в тысяча девятьсот двадцать четвёртом — книгу «Стиль и эпоха». В ней Гинзбург прогнозирует пути развития современной архитектуры, её связи с техническим прогрессом и социальными изменениями. Эти работы во многом способствовали созданию теоретической основы конструктивизма, который вскоре оформился как самостоятельное архитектурное направление. В 1925 году, Гинзбург стал одним из организаторов Объединения современных архитекторов (ОСА), в которое вошли ведущие конструктивисты. В 1930 году после вхождения ОСА в состав МОВАНО стал членом правления новой организации. ",
    		gallery: "Добро пожаловать в виртуальный музей посвящённый конструктивизму. Мы расскажем вам про ячейки эф в двух этапах её существования. Первый этап — замысел архитектора, а второй этап, то как она была создана. Перед вами представлена 3 дэ модель, созданная по чертежам и реальным снимкам ячейки, повторяющая архитектуру и внутренний интерьер квартиры.",
    		room1: "Перед вами представлен вариант квартиры-ячейки, задуманный архитектором. Это была экспериментальная работа, инициатором которой стал лидер конструктивизма М.Я. Гинзбург. Он предложил новую типологию жилья с учетом потребности того времени — максимально-экономичное и при этом комфортное жилье для рабочих. ячейке эф очень необычная планировка. По сути, это двухуровневая квартира с лестницей. В проекте был предусмотрен санузел, расположенный на втором ярусе справа."

    	}
    	function tts(text, callback = false) {
 
    		var url = "https://tts.voicetech.yandex.net/generate?"+
    			"key=069b6659-984b-4c5f-880e-aaedcfd84102"+
    			"&text="+encodeURI(text)+
    			"&format=mp3"+
    			"&lang=ru-RU"+
    			"&speaker=erkanyavas";
    		audio.src = url;
    		audio.load();
    		audio.onloadeddata = function () {
    			audio.play();
    		}
    		audio.onended = function () {
    			if(callback) callback();
    		}
    	}
    	function switchScene(a) {
    		document.getElementById("main_scene").setAttribute("visible", false)
    		document.getElementById("second_scene").setAttribute("visible", false)

    		document.getElementById(a).setAttribute("visible", true)
    	}
    	document.querySelector('#button_bio').addEventListener('hitstart', function (evt) {
    		tts(voiceovers.bio)
		});
		document.querySelector('.hand').addEventListener('triggerup', function(event) {
			for (var i =0; i < document.getElementsByClassName("gallery_item").length; i++) {
				document.getElementsByClassName("gallery_item")[i].setAttribute("material", "opacity: 1");
			}
		  	tts(voiceovers.gallery, ()=>{
		  		var fade = new FadeInOut(1, 0, (value) => {
					for (var i =0; i < document.getElementsByClassName("gallery_item").length; i++) {
						document.getElementsByClassName("gallery_item")[i].setAttribute("material", "opacity: "+value);
					}
				}, () => {
					tts(voiceovers.room1);
				}, 2000);
				fade.anim();
		  	})
		});
    </script>

  </body>
</html>
